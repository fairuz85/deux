<?php

namespace App\Http\Controllers;


use App\Mail\ResetEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use JWTAuth;

class UsersController extends Controller
{
    private $rules;


    public function register(Request $request)
    {
        $data = $request->all();

        $this->rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ];

        $this->validate($data, $this->rules);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        //Auto-login
        if (!$token=JWTAuth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            return $this->response(false,[],'These credentials do not match our records.',401);
        }

        $data=$user->toArray();
        $token=['token'=>$token];
        $data=array_merge((array)$data,$token);

        //TODO: Send email success register

        return $this->response(true, $data);

    }

    public function login(Request $request)
    {
        $data = $request->all();

        $this->rules = [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ];

        $this->validate($data, $this->rules);

        if (!$token=JWTAuth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            return $this->response(false,[],'Invalid login, please try again.',401);
        }

        $user = User::query()->where('email', $data['email'])->first();
        $data=$user->toArray();
        $token=['token'=>$token];
        $data=array_merge((array)$data,$token);

        return $this->response(true, $data);
    }

    public function logout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());

        return $this->response(true, [],"Logout successfully");
    }

    public function resetPassword(Request $request)
    {
        $data = $request->all();

        $this->rules = [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ];

        $this->validate($data, $this->rules);

        $user = User::query()->where('email', $data['email'])->first()->toArray();

        if(count($user)<=0){
            return $this->response(false,[],'User not found.',400);
        }

        $user->password=Hash::make($data['password']);
        $user->save();

        //Mail::to($user['email'])->send(new ResetEmail());

        return $this->response(true, [],"Password reset successfully");
    }


    public function create(Request $request)
    {
        $data = $request->all();

        $this->rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ];

        $this->validate($data, $this->rules);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return $this->response(true, $user->toArray(), "User create successfully");

    }


    public function read($id)
    {
        $this->rules = [
            'id' => ['required', 'numeric', 'min:1'],
        ];

        $this->validate(['id'=>$id], $this->rules);

        $user = User::query()->where('id',$id)->first()->toArray();

        if(is_null($user)){
            return $this->response(false,[],'User not found.',400);
        }

        return $this->response(true, $user,"Read user successfully");
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $this->rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ];

        $this->validate($data, $this->rules);

        $user = User::query()->where('email', $data['email'])->first();

        if(is_null($user)){
            return $this->response(false,[],'User not found.',400);
        }

        $user->name=$data['name'];
        $user->email=$data['email'];
        $user->password = Hash::make($data['password']);
        $user->save();

        return $this->response(true, [],"Update user successfully");

    }

    public function delete($id)
    {

        $this->rules = [
            'id' => ['required', 'numeric', 'min:1'],
        ];

        $this->validate(['id'=>$id], $this->rules);

        User::query()->where('id',$id)->delete();

        return $this->response(true, [],"User deleted successfully");
    }

    public function list(Request $request)
    {

        //TODO: Add filter & pagination handling

        $user = User::all()->toArray();

        return $this->response(true, $user,"Retrieved user list");
    }
}
