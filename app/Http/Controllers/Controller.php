<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Exceptions\HttpResponseException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response($success=true,$data=[],$message="",$http_code=200){
        $res = [
            'success'=>$success,
            'data'=>$data,
            'message'=>$message,
        ];
        return new JsonResponse($res,$http_code);
    }

    public function validate($data, $rules, $messages=[])
    {
        $validator = Validator::make($data, $rules, $messages);

        if($validator->fails()) {
            throw new HttpResponseException(
                $this->response(false,[],'Validation error : '.$validator->messages()->first(),422)
            );

        }

        return true;
    }

}
