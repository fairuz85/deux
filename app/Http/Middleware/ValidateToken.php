<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use JWTAuth;

class ValidateToken
{

    /**
     * Checks if jwt token is valid.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $exception) {
            throw new \Illuminate\Http\Exceptions\HttpResponseException(response($exception->getMessage(),401));
        }

        return $next($request);
    }
}
