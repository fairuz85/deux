<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use JWTAuth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {

        if (! $request->expectsJson()) {
            return route('login');
        }
//        try {
//
//            $user = JWTAuth::parseToken()->authenticate();
//            dd($user);
//        } catch (Exception $exception) {
//            throw new \Illuminate\Http\Exceptions\HttpResponseException(response($exception->getMessage(),401));
//        }


    }
}
