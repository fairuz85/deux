<?php
namespace App\Services\Auth;

use App\User;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWT;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class JWTGuard extends Auth
{
    use GuardHelpers;
    use AuthenticatesUsers;

    /**
     * @var JWT $jwt
     */
    protected  $jwt;
    /**
     * @var Request $request
     */
    protected  $request;
    /**
     * JWTGuard constructor.
     * @param JWT $jwt
     * @param Request $request
     */
    public function __construct(JWT $jwt, Request $request) {
        $this->jwt = $jwt;
        $this->request = $request;
    }
    public function user() {
        if (! is_null($this->user)) {
            return $this->user;
        }
        if ($this->jwt->setRequest($this->request)->getToken() && $this->jwt->check()) {
            $id = $this->jwt->payload()->get('sub');
            $this->user = new User();
            $this->user->id = $id;
            // Set data from custom claims
            return $this->user;
        }
        return null;
    }
    public function validate(array $credentials = []) {
    }
}