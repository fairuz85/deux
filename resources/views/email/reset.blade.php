<!doctype html>
<html lang="en">
<head>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <style>
        body {
            margin: 0 !important;
            padding: 0 !important;
            font-family: 'Lato', sans-serif;
            font-size: 16px;
        }
        .font-lato {
            font-family: 'Lato', sans-serif;
            font-weight: 400;
        }
        .background {
            background-blend-mode: multiply; -webkit-background-size: cover;
            background-size: cover;
        }
        .container {
            width: 600px;
        }
        .sm-hide {
            display: block;
        }
        @media only screen and (max-width: 600px) {
            body {
                font-size: 10px;
            }
            .container {
                width: 100%;
            }
            .sm-hide {
                display: none;
            }
        }
        .content {
            width: 100%;
            padding: 30px;
            box-sizing: border-box;
        }
        a {
            color: #009add;
        }
        .btn {
            padding: 15px 2em;
        }
        .bg-primary {
            background-color: #00baa4;
        }
        .bg-white {
            background-color: #FFFFFF;
        }
        .align-center {
            margin: auto;
        }
        .text-center {
            text-align: center;
        }
        .text-left {
            text-align: left;
        }
        .w-100 {
            width: 100%;
        }
        .all-caps {
            text-transform: uppercase;
        }
        .hint-text {
            opacity: 0.7;
        }
        .muted {
            color: #9c9c9c;
        }
        .text-white {
            color: #FFFFFF;
        }
        .text-primary {
            color: #00baa4;
        }
        .pointer {
            cursor: pointer;
        }
        .text-complete {
            color: #009add;
        }
        .lightest {
            font-weight: 100 !important;
        }
        .light {
            font-weight: 300 !important;
        }
        .normal {
            font-weight: 400 !important;
        }
        .bold {
            font-weight: 700 !important;
        }
        .bolder {
            font-weight: 900 !important;
        }
        .no-border {
            border: none !important;
        }
        .no-padding {
            padding: 0 !important;
        }
        .no-margin {
            margin: 0 !important;
        }
    </style>
</head>
<body>
<div class="w-100">
    <div class="background text-center">
        <div class="sm-hide" style="height: 50px;"></div>
        <div class="container align-center">

            <div class="bg-white text-left">
                <div class="content">

                </div>
            </div>
        </div>
        <div class="text-center text-white all-caps" style="margin-top: 30px;">
            <small style="font-size: 0.6em; letter-spacing: 3px;">&copy;2021. All rights reserved</small>
        </div>
        <div style="height: 30px;"></div>
    </div>
    //TODO: add controlel
</div>
</body>
</html>