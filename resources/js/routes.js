import Register from './components/Register.vue';
import ResetPassword from './components/ResetPassword.vue';
import Login from './components/Login.vue';
import Dashboard from './components/Dashboard.vue';
import Create from './components/Create.vue';
import Read from './components/Read.vue';
import Update from './components/Update.vue';

export const routes = [
    {
        name: 'login',
        path: '/',
        component: Login
    },
    {
        name: 'register',
        path: '/register',
        component: Register
    },
    {
        name: 'reset',
        path: '/reset-password',
        component: ResetPassword
    },
    {
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard
    },
    {
        name: 'create',
        path: '/create',
        component: Create
    },
    {
        name: 'read',
        path: '/read/:id',
        component: Read
    },
    {
        name: 'update',
        path: '/update/:id',
        component: Update
    },
];