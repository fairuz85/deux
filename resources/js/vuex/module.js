import API from '../network/api';

let user = JSON.parse(localStorage.getItem('user'));
let initialState = user
    ? { status: { loggedIn: true }, user }
    : { status: { loggedIn: false }, user: null };

export const module = {
    namespaced: true,
    state: initialState,
    actions: {
        login({ commit }, user) {
            return API.login(user).then(
                response => {
                    commit('loginSuccess', user);
                    return Promise.resolve(user);
                },
                error => {
                    commit('loginFailure');
                    return Promise.reject(error.response.data);
                }
            );
        },
        logout({ commit }) {
            API.logout();
            commit('logout');
        },
        register({ commit }, user) {
            return API.register(user).then(
                response => {
                    commit('registerSuccess');
                    return Promise.resolve(response.data);
                },
                error => {
                    commit('registerFailure');
                    return Promise.reject(error.response.data);
                }
            );
        },
        resetPassword({ commit }, user) {
            return API.resetPassword(user).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {
                    return Promise.reject(error.response.data);
                }
            );
        },
        list({ commit }, {}) {
            return API.list({}).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {
                    if(error.response.status==401) commit('authFailed');
                    return Promise.reject(error.response.data);
                }
            );
        },
        create({ commit }, user) {
            return API.create(user).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {
                    if(error.response.status==401) commit('authFailed');
                    return Promise.reject(error.response.data);
                }
            );
        },
        read({ commit }, id) {
            return API.read(id).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {
                    if(error.response.status==401) commit('authFailed');
                    return Promise.reject(error.response.data);
                }
            );
        },
        update({ commit }, user) {
            return API.update(user).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {

                    if(error.response.status==401) commit('authFailed');
                    return Promise.reject(error.response.data);
                }
            );
        },
        delete({ commit }, id) {
            return API.delete(id).then(
                response => {
                    return Promise.resolve(response.data);
                },
                error => {
                    if(error.response.status==401) commit('authFailed');
                    return Promise.reject(error.response.data);
                }
            );
        }
    },
    mutations: {
        loginSuccess(state, user) {
            state.status.loggedIn = true;
            state.user = user;
        },
        loginFailure(state) {
            state.status.loggedIn = false;
            state.user = null;
        },
        authFailed(state) {
            state.status.loggedIn = false;
            state.user = null;
        },
        logout(state) {
            state.status.loggedIn = false;
            state.user = null;
        },
        registerSuccess(state) {
            state.status.loggedIn = true;
        },
        registerFailure(state) {
            state.status.loggedIn = false;
        }
    }
};