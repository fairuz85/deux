import axios from 'axios';

const API_URL = window.Laravel.baseUrl+'/';
const ROUTES = window.Laravel.routes;

class API {
    getHeader(){
        let config={};
        let user = JSON.parse(localStorage.getItem('user'));
        let headers={ 'Content-Type': 'application/json'};

        if (user && user.token) {
            headers['Authorization'] = 'Bearer ' + user.token;
        } else {
            headers = {};
        }

        return config = {
            headers:headers
        }
    }

    login(user) {
        return axios
            .post(API_URL + ROUTES['users.login'], {
                email: user.email,
                password: user.password
            }, this.getHeader())
            .then(response => {
                if (response.data.data) {
                    localStorage.setItem('user', JSON.stringify(response.data.data));
                }

                return response.data;
            });
    }

    logout() {
        return axios
            .get(API_URL + ROUTES['users.logout'], this.getHeader())
            .then(response => {
                localStorage.removeItem('user');

                return response.data;
            });

    }

    register(user) {
        return axios.post(API_URL + ROUTES['users.register'], {
            name: user.name,
            email: user.email,
            password: user.password
        }).then(response => {
            if (response.data.data) {
                localStorage.setItem('user', JSON.stringify(response.data.data));
            }

            console.log(response);
            return response.data;
        });
    }

    resetPassword(user) {
        return axios.post(API_URL + ROUTES['users.reset'], {
            name: user.name,
            email: user.email,
            password: user.password
        }).then(response => {
            if (response.data.user) {
                localStorage.setItem('user', JSON.stringify(response.data.user));
            }

            console.log(response);
            return response.data;
        });
    }

    create(user) {
        return axios.post(API_URL + ROUTES['users.create'], {
            name: user.name,
            email: user.email,
            password: user.password
        }, this.getHeader()).then(response => {
            console.log(response);
            return response.data;
        });
    }

    read(id) {
        return axios.get(API_URL + ROUTES['users.read']+id, this.getHeader()).then(response => {
            console.log(response);
            return response.data;
        });;
    }

    update(user) {
        return axios.put(API_URL + ROUTES['users.update'], {
            name: user.name,
            email: user.email,
            password: user.password
        }, this.getHeader()).then(response => {
            console.log(response);
            return response.data;
        });
    }

    delete(id) {
        return axios.delete(API_URL + ROUTES['users.delete']+id, this.getHeader()).then(response => {
            console.log(response);
            return response.data;
        });
    }

    list(filter) {
        return axios.post(API_URL + ROUTES['users.list'], filter, this.getHeader()).then(response => {
            console.log(response);
            return response.data;
        });
    }
}

export default new API();