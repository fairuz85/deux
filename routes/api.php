<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'users'], function() {
    Route::post('register', 'UsersController@register')->name("users.register");
    Route::post('login', 'UsersController@login')->name("users.login");
    Route::post('reset', 'UsersController@resetPassword')->name("users.reset");
    Route::get('logout', 'UsersController@logout')->name("users.logout");
});



    Route::group(['prefix' => 'users','middleware' => 'validate.token'], function () {
        Route::post('create', 'UsersController@create')->name("users.create");
        Route::get('read/{id}', 'UsersController@read')->name("users.read");
        Route::put('update', 'UsersController@update')->name("users.update");
        Route::delete('delete/{id}', 'UsersController@delete')->name("users.delete");
        Route::post('list', 'UsersController@list')->name("users.list");
    });


